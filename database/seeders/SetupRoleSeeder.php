<?php
/*
 * Author Dede Fajriansyah
 * Created on Sunday September 27th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace Database\Seeders;

use App\Enums\Users\UserRole;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class SetupRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (UserRole::getValues() as $role) {
            Role::create(['name' => $role]);
        }
    }
}
