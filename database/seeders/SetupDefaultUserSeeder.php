<?php
/*
 * Author Dede Fajriansyah
 * Created on Monday September 28th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Contracts\Users\UserContract;
use App\Enums\Users\UserRole;

class SetupDefaultUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = resolve(UserContract::class)->create([
            "name" => "SuperAdmin",
            "email" => "superadmin@fajriansyah.id",
            "password" => Hash::make("superzone"),
            "email_verified_at" => now(),
        ]);
        $user->assignRole(UserRole::getValues());
    }
}
