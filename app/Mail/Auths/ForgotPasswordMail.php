<?php
/*
 * Author Dede Fajriansyah
 * Created on Monday September 28th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Mail\Auths;

use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\Users\PasswordReset;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var \App\Models\Users\User
     */
    protected $user;

    /**
     * @var \App\Models\Users\PasswordReset
     */
    protected $passwordReset;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param PasswordReset $passwordReset
     * @return void
     */
    public function __construct(User $user, PasswordReset $passwordReset)
    {
        $this->user = $user;
        $this->passwordReset = $passwordReset;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Did you forget your password for " . Config::get("app.name") . "?";
        $url = Config::get("app.web_url") . "/password-resets?&token=" . $this->passwordReset->token . "&email=" . $this->passwordReset->email;
        return $this
            ->subject($subject)
            ->markdown('emails.auths.forgot-password', [
                "url" => $url
            ]);
    }
}
