<?php
/*
 * Author Dede Fajriansyah
 * Created on Sunday September 27th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Enums\Users;

use BenSampo\Enum\Enum;

final class UserRole extends Enum
{
    const SUPERADMIN = "SUPERADMIN";
    const MEMBER = "MEMBER";
}
