<?php
/*
 * Author Dede Fajriansyah
 * Created on Thursday September 17th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Repositories\Users;

use App\Contracts\Users\PasswordResetContract;
use App\Models\Users\User;
use Illuminate\Support\Str;
use App\Enums\Users\UserRole;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Contracts\Users\UserContract;
use App\Mail\Auths\ForgotPasswordMail;

class UserRepository extends BaseRepository implements UserContract
{
    /**
     * Constructor of BaseRepository
     */
    public function __construct()
    {
        $this->setModel(new User());
    }

    /**
     * Create user by administrator.
     *
     * @param array $request
     * @return void
     */
    public function createByAdministrator(array $request)
    {
        $request["password"] = Hash::make(Str::random(10));
        return $this->model->create($request);
    }

    /**
     * Create user by manuall register.
     *
     * @param array $request
     * @return void
     */
    public function createByRegister(array $request)
    {
        $request["password"] = Hash::make($request["password"]);
        $user = $this->model->create($request);
        $user->assignRole(UserRole::MEMBER);
        return $user;
    }

    /**
     * Create user access token.
     *
     * @param User $user
     * @return void
     */
    public function createAccessToken(User $user)
    {
        return $user->createToken($user->name, $user->roles()->pluck("name")->toArray());
    }

    /**
     * Get List
     *
     * @param array $request
     * @return void
     */
    public function paginate(array $request, array $with = [], array $withCount = [])
    {
        $request["perPage"] = $request["perPage"] ?? 20;
        $request["search"] = $request["search"] ?? null;

        return $this->model
            ->defaultSelect()
            ->with($with)
            ->withCount($withCount)
            ->where(function ($query) use ($request) {
                $query
                    ->when(in_array(strtoupper(@$request["role"]), UserRole::getValues()), function ($query) use ($request) {
                        $query->role($request["role"]);
                    })
                    ->when(!is_null($request["search"]), function ($query) use ($request) {
                        $query
                            ->where("name", "LIKE", "%" . $request["search"] . "%")
                            ->orWhere("email", "=", $request["search"]);
                    });
            })
            ->paginate($request["perPage"]);
    }

    /**
     * Syncronizing user roles.
     *
     * @param User $user
     * @param array $roles
     * @return void
     */
    public function setRoles(User $user, array $roles)
    {
        return $user->syncRoles($roles);
    }

    /**
     * Change user password.
     *
     * @param User $user
     * @param array $request
     * @param bool $withOldPasswordCheck
     * @return void
     */
    public function changePassword(User $user, array $request, bool $withOldPasswordCheck = true)
    {
        if ($withOldPasswordCheck && !Hash::check($request["old_password"], $user->password)) {
            throw new \Exception("Old password doesn't match!");
        }

        $request["password"] = Hash::make($request["password"]);
        $user->fill($request);
        $user->save();
        return $user;
    }

    /**
     * Send email for forgot password link.
     *
     * @param string $email
     * @return void
     */
    public function sendForgotPasswordLink(User $user)
    {
        $passwordReset = resolve(PasswordResetContract::class)->createToken($user);
        return Mail::to($user)->send(new ForgotPasswordMail($user, $passwordReset));
    }
}
