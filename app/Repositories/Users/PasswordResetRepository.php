<?php
/*
 * Author Dede Fajriansyah
 * Created on Monday September 28th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Repositories\Users;

use App\Models\Users\User;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use App\Models\Users\PasswordReset;
use App\Repositories\BaseRepository;
use App\Exceptions\UnprocessableException;
use App\Contracts\Users\PasswordResetContract;

class PasswordResetRepository extends BaseRepository implements PasswordResetContract
{
    /**
     * Constructor of BaseRepository
     */
    public function __construct()
    {
        $this->setModel(new PasswordReset());
    }

    /**
     * Create password reset token.
     *
     * @param User $user
     * @return void
     */
    public function createToken(User $user)
    {
        return $user->passwordResets()->create([
            "token" => Str::random(97),
            "expired_at" => Carbon::now()->addMinutes(5),
        ]);
    }

    /**
     * Check token is valid or not.
     *
     * @param array $request
     * @return boolean
     */
    public function isValidToken(array $request)
    {
        $count = $this->model
            ->defaultSelect()
            ->isNotExpired()
            ->where($request)
            ->count("email");

        if ($count > 0) {
            return $count;
        }

        throw new UnprocessableException("Invalid Token!");
    }
}
