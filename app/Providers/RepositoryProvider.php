<?php
/*
 * Author Dede Fajriansyah
 * Created on Thursday September 17th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Providers;

use App\Contracts\Users\UserContract;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Users\UserRepository;
use App\Contracts\Users\PasswordResetContract;
use App\Repositories\Users\PasswordResetRepository;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserContract::class, UserRepository::class);
        $this->app->bind(PasswordResetContract::class, PasswordResetRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
