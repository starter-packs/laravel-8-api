<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend("enum", function ($attribute, $value, $parameters, $validator) {
            if (count($parameters) === 0) {
                return false;
            }

            if (!class_exists($parameters[0])) {
                return false;
            }

            return in_array(strtoupper($value), $parameters[0]::getValues()) ? true : false;
        });
    }
}
