<?php
/*
 * Author Dede Fajriansyah
 * Created on Saturday February 22nd 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

if (!function_exists("rules_enum")) {
    function rules_enum($enumNamespace = null)
    {
        if (!is_null($enumNamespace)) {
            return implode(",", $enumNamespace::getValues());
        }

        return null;
    }
}

if (!function_exists("price_format")) {
    function price_format(int $price = null)
    {
        if (is_null($price)) {
            $price = 0;
        }

        return "IDR " . number_format($price, 0, ",", ".");
    }
}
