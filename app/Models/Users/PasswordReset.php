<?php
/*
 * Author Dede Fajriansyah
 * Created on Monday September 28th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Models\Users;

use Carbon\Carbon;
use App\Traits\ModelDefaultSelect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PasswordReset extends Model
{
    use HasFactory, ModelDefaultSelect;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "password_resets";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "email",
        "token",
        "code",
        "expired_at",
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        "email" => "string",
        "token" => "string",
        "code" => "integer",
        "expired_at" => "datetime",
    ];

    /**
     * Scope a query to only include isNotExpired
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsNotExpired($query)
    {
        return $query->where("expired_at", ">", Carbon::now());
    }
}
