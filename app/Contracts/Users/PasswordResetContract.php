<?php
/*
 * Author Dede Fajriansyah
 * Created on Monday September 28th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Contracts\Users;

use App\Models\Users\User;
use App\Contracts\BaseContract;

interface PasswordResetContract extends BaseContract
{
    public function createToken(User $user);
    public function isValidToken(array $request);
}
