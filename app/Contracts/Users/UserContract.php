<?php
/*
 * Author Dede Fajriansyah
 * Created on Thursday September 17th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Contracts\Users;

use App\Models\Users\User;
use App\Contracts\BaseContract;

interface UserContract extends BaseContract
{
    public function createByAdministrator(array $request);
    public function createByRegister(array $request);
    public function createAccessToken(User $user);
    public function setRoles(User $user, array $roles);
    public function changePassword(User $user, array $request, bool $withOldPasswordCheck = true);
    public function sendForgotPasswordLink(User $user);
}
