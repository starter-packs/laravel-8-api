<?php
/*
 * Author Dede Fajriansyah
 * Created on Monday September 28th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Http\Requests\API\V1\Auths;

use App\Http\Requests\API\V1\CustomFormRequest;

class StoreChangePasswordRequest extends CustomFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "old_password" => "required|string|min:6",
            "password" => "required|string|min:6|confirmed|different:old_password",
        ];
    }
}
