<?php
/*
 * Author Dede Fajriansyah
 * Created on Sunday September 27th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Http\Requests\API\V1\Users;

use App\Http\Requests\API\V1\CustomFormRequest;

class StoreUserRequest extends CustomFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string|max:50",
            "email" => "required|email|unique:users,email",
            "roles" => "required|array|min:1",
            "roles.*" => "required|string|enum:\App\Enums\Users\UserRole"
        ];
    }
}
