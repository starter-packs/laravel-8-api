<?php
/*
 * Author Dede Fajriansyah
 * Created on Sunday September 27th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Http\Controllers\API\V1\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Users\UserContract;
use App\Http\Resources\API\V1\Users\UserResource;
use App\Http\Requests\API\V1\Users\StoreUserRequest;
use App\Http\Requests\API\V1\Users\UpdateUserRequest;

class UserController extends Controller
{
    /**
     * @var \App\Contracts\Users\UserContract
     */
    protected $userContract;

    /**
     * Login Controller
     *
     * @param UserContract $userContract
     */
    public function __construct(UserContract $userContract)
    {
        $this->userContract = $userContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $searchable = ["perPage", "search", "role"];
            $users = $this->userContract->paginate($request->only($searchable), ["roles"]);
            foreach ($users as $key => $user) {
                $users[$key] = new UserResource($user);
            }
            return $this->apiSuccessResponse("Success!", [
                "users" => $users
            ]);
        } catch (\Throwable $th) {
            return $this->apiErrorResponse($th);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\API\V1\Users\StoreUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        try {
            $fillable = ["name", "email"];
            $user = $this->userContract->createByAdministrator($request->only($fillable));
            $this->userContract->setRoles($user, $request->input("roles"));

            return $this->apiSuccessResponse("Success!", [
                "user" => new UserResource($user)
            ]);
        } catch (\Throwable $th) {
            return $this->apiErrorResponse($th);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = $this->userContract->findBy("id", $id, "=", ["roles"]);
            return $this->apiSuccessResponse("Success!", [
                "user" => new UserResource($user)
            ]);
        } catch (\Throwable $th) {
            return $this->apiErrorResponse($th);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\API\V1\Users\UpdateUserRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        try {
            $fillable = ["name", "email"];
            $user = $this->userContract->update($id, $request->only($fillable));
            $this->userContract->setRoles($user, $request->input("roles"));

            return $this->apiSuccessResponse("Success!", [
                "user" => new UserResource($user)
            ]);
        } catch (\Throwable $th) {
            return $this->apiErrorResponse($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->userContract->delete($id);

            return $this->apiSuccessResponse("Success!", [
                "user" => null
            ]);
        } catch (\Throwable $th) {
            return $this->apiErrorResponse($th);
        }
    }
}
