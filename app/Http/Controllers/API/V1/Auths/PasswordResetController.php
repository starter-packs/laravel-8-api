<?php
/*
 * Author Dede Fajriansyah
 * Created on Monday September 28th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Http\Controllers\API\V1\Auths;

use App\Contracts\Users\PasswordResetContract;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Users\UserContract;
use App\Http\Requests\API\V1\Auths\StorePasswordResetRequest;

class PasswordResetController extends Controller
{
    /**
     * @var \App\Contracts\Users\UserContract
     */
    protected $userContract;

    /**
     * @var \App\Contracts\Users\PasswordResetContract
     */
    protected $passwordResetContract;

    /**
     * PasswordResetController
     *
     * @param UserContract $userContract
     * @param PasswordResetContract $passwordResetContract
     */
    public function __construct(UserContract $userContract, PasswordResetContract $passwordResetContract)
    {
        $this->userContract = $userContract;
        $this->passwordResetContract = $passwordResetContract;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\API\V1\Auths\StorePasswordResetRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePasswordResetRequest $request)
    {
        try {
            $this->passwordResetContract->isValidToken($request->only(["email", "token"]));
            $user = $this->userContract->findBy("email", $request->input("email"));
            $this->userContract->changePassword($user, $request->only("password"), false);
            return $this->apiSuccessResponse("Success!");
        } catch (\Throwable $th) {
            return $this->apiErrorResponse($th);
        }
    }
}
