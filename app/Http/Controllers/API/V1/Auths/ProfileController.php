<?php
/*
 * Author Dede Fajriansyah
 * Created on Sunday September 27th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Http\Controllers\API\V1\Auths;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Contracts\Users\UserContract;
use App\Http\Resources\API\V1\Users\UserResource;
use App\Http\Requests\API\V1\Auths\StoreProfileRequest;

class ProfileController extends Controller
{
    /**
     * @var \App\Contracts\Users\UserContract
     */
    protected $userContract;

    /**
     * Login Controller
     *
     * @param UserContract $userContract
     */
    public function __construct(UserContract $userContract)
    {
        $this->userContract = $userContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return $this->apiSuccessResponse("Success!", [
                "user" => new UserResource(Auth::user()),
            ]);
        } catch (\Throwable $th) {
            return $this->apiErrorResponse($th);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\API\V1\Users\StoreProfileRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProfileRequest $request)
    {
        try {
            $fillable = ["name", "email"];
            $user = $this->userContract->update(Auth::id(), $request->only($fillable));

            return $this->apiSuccessResponse("Success!", [
                "user" => new UserResource($user)
            ]);
        } catch (\Throwable $th) {
            return $this->apiErrorResponse($th);
        }
    }
}
