<?php
/*
 * Author Dede Fajriansyah
 * Created on Sunday September 27th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Http\Controllers\API\V1\Auths;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Users\UserContract;
use App\Http\Resources\API\V1\Users\UserResource;
use App\Http\Requests\API\V1\Auths\StoreLoginRequest;
use App\Http\Resources\API\V1\Users\AccessTokenResource;

class LoginController extends Controller
{
    /**
     * @var \App\Contracts\Users\UserContract
     */
    protected $userContract;

    /**
     * Login Controller
     *
     * @param UserContract $userContract
     */
    public function __construct(UserContract $userContract)
    {
        $this->userContract = $userContract;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\API\V1\Auths\StoreLoginRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLoginRequest $request)
    {
        try {
            $user = $this->userContract->findBy("email", $request->input("email"));
            $accessToken = $this->userContract->createAccessToken($user);
            return $this->apiSuccessResponse("Success!", [
                "user" => new UserResource($user),
                "auth" => new AccessTokenResource($accessToken)
            ]);
        } catch (\Throwable $th) {
            return $this->apiErrorResponse($th);
        }
    }
}
