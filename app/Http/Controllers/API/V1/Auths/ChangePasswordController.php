<?php
/*
 * Author Dede Fajriansyah
 * Created on Monday September 28th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Http\Controllers\API\V1\Auths;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Contracts\Users\UserContract;
use App\Http\Resources\API\V1\Users\UserResource;
use App\Http\Requests\API\V1\Auths\StoreChangePasswordRequest;

class ChangePasswordController extends Controller
{
    /**
     * @var \App\Contracts\Users\UserContract
     */
    protected $userContract;

    /**
     * Login Controller
     *
     * @param UserContract $userContract
     */
    public function __construct(UserContract $userContract)
    {
        $this->userContract = $userContract;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\API\V1\Auths\StoreChangePasswordRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreChangePasswordRequest $request)
    {
        try {
            $fillable = ["old_password", "password"];
            $this->userContract->changePassword($request->user(), $request->only($fillable));

            return $this->apiSuccessResponse("Success!", [
                "user" => new UserResource(Auth::user()),
            ]);
        } catch (\Throwable $th) {
            return $this->apiErrorResponse($th);
        }
    }
}
