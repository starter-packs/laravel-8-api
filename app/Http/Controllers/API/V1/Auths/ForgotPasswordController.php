<?php
/*
 * Author Dede Fajriansyah
 * Created on Monday September 28th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Http\Controllers\API\V1\Auths;

use App\Contracts\Users\PasswordResetContract;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Users\UserContract;
use App\Http\Requests\API\V1\Auths\StoreForgotPasswordRequest;

class ForgotPasswordController extends Controller
{
    /**
     * @var \App\Contracts\Users\UserContract
     */
    protected $userContract;

    /**
     * Login Controller
     *
     * @param UserContract $userContract
     */
    public function __construct(UserContract $userContract)
    {
        $this->userContract = $userContract;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\API\V1\Auths\StoreForgotPasswordRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreForgotPasswordRequest $request)
    {
        try {
            $user = $this->userContract->findBy("email", $request->input("email"));
            $this->userContract->sendForgotPasswordLink($user);
            return $this->apiSuccessResponse("Success!");
        } catch (\Throwable $th) {
            return $this->apiErrorResponse($th);
        }
    }
}
