<?php
/*
 * Author Dede Fajriansyah
 * Created on Sunday September 27th 2020
 * Email dede.fajriansyah97@gmail.com
 * Telegram https://t.me/dedefajriansyah
 *
 * Copyright (c) 2020 FAJRIANSYAH.COM
 */

namespace App\Http\Resources\API\V1\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class AccessTokenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "access_token" => $this->accessToken,
            "revoked" => $this->token->revoked,
            "expires_at" => $this->token->expires_at,
        ];
    }
}
