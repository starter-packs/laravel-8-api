<?php

use App\Http\Controllers\API\V1\Auths\ChangePasswordController;
use App\Http\Controllers\API\V1\Auths\ForgotPasswordController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\V1\Users\UserController;
use App\Http\Controllers\API\V1\Auths\LoginController;
use App\Http\Controllers\API\V1\Auths\PasswordResetController;
use App\Http\Controllers\API\V1\Auths\ProfileController;
use App\Http\Controllers\API\V1\Auths\RegisterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix("v1")->group(function () {
    Route::prefix("auth")->group(function () {
        Route::apiResource("register", RegisterController::class)->only(["store"]);
        Route::apiResource("access-tokens", LoginController::class)->only(["store"]);
        Route::apiResource("forgot-passwords", ForgotPasswordController::class)->only(["store"]);
        Route::apiResource("password-resets", PasswordResetController::class)->only(["store"]);
    });
    Route::middleware(["auth:api"])->group(function () {
        Route::apiResource("profiles", ProfileController::class)->only(["index", "store"]);
        Route::apiResource("change-passwords", ChangePasswordController::class)->only(["store"]);
        Route::apiResource("users", UserController::class);
    });
});
