@component('mail::message')
# Don't worry, we've got your back.

Reset your password to make it more secure and easier to remember.

@component('mail::button', ['url' => $url])
Reset My Password
@endcomponent

If you don't do this you can ignore this email.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
